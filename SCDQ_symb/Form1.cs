﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SCDQ_symb
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cb00_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0;
            val = cb00.Checked ? 16 + val : val;
            val = cb01.Checked ? 8 + val : val;
            val = cb02.Checked ? 4 + val : val;
            val = cb03.Checked ? 2 + val : val;
            val = cb04.Checked ? 1 + val : val;
            tb0.Text = "0x" + val.ToString("X2");
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            cb00.Checked = false;
            cb01.Checked = false;
            cb02.Checked = false;
            cb03.Checked = false;
            cb04.Checked = false;

            cb10.Checked = false;
            cb11.Checked = false;
            cb12.Checked = false;
            cb13.Checked = false;
            cb14.Checked = false;

            cb20.Checked = false;
            cb21.Checked = false;
            cb22.Checked = false;
            cb23.Checked = false;
            cb24.Checked = false;

            cb30.Checked = false;
            cb31.Checked = false;
            cb32.Checked = false;
            cb33.Checked = false;
            cb34.Checked = false;

            cb40.Checked = false;
            cb41.Checked = false;
            cb42.Checked = false;
            cb43.Checked = false;
            cb44.Checked = false;
        }

        private void cb10_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0x20;
            val = cb10.Checked ? 16 + val : val;
            val = cb11.Checked ? 8 + val : val;
            val = cb12.Checked ? 4 + val : val;
            val = cb13.Checked ? 2 + val : val;
            val = cb14.Checked ? 1 + val : val;
            tb1.Text = "0x" + val.ToString("X2");
        }

        private void cb20_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0x40;
            val = cb20.Checked ? 16 + val : val;
            val = cb21.Checked ? 8 + val : val;
            val = cb22.Checked ? 4 + val : val;
            val = cb23.Checked ? 2 + val : val;
            val = cb24.Checked ? 1 + val : val;
            tb2.Text = "0x" + val.ToString("X2");
        }

        private void cb30_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0x60;
            val = cb30.Checked ? 16 + val : val;
            val = cb31.Checked ? 8 + val : val;
            val = cb32.Checked ? 4 + val : val;
            val = cb33.Checked ? 2 + val : val;
            val = cb34.Checked ? 1 + val : val;
            tb3.Text = "0x" + val.ToString("X2");
        }

        private void cb40_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0x80;
            val = cb40.Checked ? 16 + val : val;
            val = cb41.Checked ? 8 + val : val;
            val = cb42.Checked ? 4 + val : val;
            val = cb43.Checked ? 2 + val : val;
            val = cb44.Checked ? 1 + val : val;
            tb4.Text = "0x" + val.ToString("X2");
        }

        private void tb0_TextChanged(object sender, EventArgs e)
        {
            textBoxChar.Text =  tb0.Text + ", " + tb1.Text + ", " + tb2.Text + ", " + tb3.Text + ", " + tb4.Text + ",";
            // For this example, the data to be placed on the clipboard is a simple
            // string.
            string textData = "\n\t" + textBoxChar.Text;

            // After this call, the data (string) is placed on the clipboard and tagged
            // with a data format of "Text".
            Clipboard.SetData(DataFormats.Text, (Object)textData);
        }

        


    }
}
