﻿namespace SCDQ_symb
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb0 = new System.Windows.Forms.TextBox();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tb4 = new System.Windows.Forms.TextBox();
            this.textBoxChar = new System.Windows.Forms.TextBox();
            this.btReset = new System.Windows.Forms.Button();
            this.cb20 = new System.Windows.Forms.CheckBox();
            this.cb40 = new System.Windows.Forms.CheckBox();
            this.cb30 = new System.Windows.Forms.CheckBox();
            this.cb10 = new System.Windows.Forms.CheckBox();
            this.cb44 = new System.Windows.Forms.CheckBox();
            this.cb42 = new System.Windows.Forms.CheckBox();
            this.cb34 = new System.Windows.Forms.CheckBox();
            this.cb32 = new System.Windows.Forms.CheckBox();
            this.cb14 = new System.Windows.Forms.CheckBox();
            this.cb24 = new System.Windows.Forms.CheckBox();
            this.cb43 = new System.Windows.Forms.CheckBox();
            this.cb12 = new System.Windows.Forms.CheckBox();
            this.cb33 = new System.Windows.Forms.CheckBox();
            this.cb04 = new System.Windows.Forms.CheckBox();
            this.cb22 = new System.Windows.Forms.CheckBox();
            this.cb41 = new System.Windows.Forms.CheckBox();
            this.cb13 = new System.Windows.Forms.CheckBox();
            this.cb31 = new System.Windows.Forms.CheckBox();
            this.cb02 = new System.Windows.Forms.CheckBox();
            this.cb23 = new System.Windows.Forms.CheckBox();
            this.cb11 = new System.Windows.Forms.CheckBox();
            this.cb21 = new System.Windows.Forms.CheckBox();
            this.cb03 = new System.Windows.Forms.CheckBox();
            this.cb01 = new System.Windows.Forms.CheckBox();
            this.cb00 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tb0);
            this.panel1.Controls.Add(this.tb1);
            this.panel1.Controls.Add(this.tb2);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.tb3);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.tb4);
            this.panel1.Controls.Add(this.textBoxChar);
            this.panel1.Controls.Add(this.btReset);
            this.panel1.Controls.Add(this.cb20);
            this.panel1.Controls.Add(this.cb40);
            this.panel1.Controls.Add(this.cb30);
            this.panel1.Controls.Add(this.cb10);
            this.panel1.Controls.Add(this.cb44);
            this.panel1.Controls.Add(this.cb42);
            this.panel1.Controls.Add(this.cb34);
            this.panel1.Controls.Add(this.cb32);
            this.panel1.Controls.Add(this.cb14);
            this.panel1.Controls.Add(this.cb24);
            this.panel1.Controls.Add(this.cb43);
            this.panel1.Controls.Add(this.cb12);
            this.panel1.Controls.Add(this.cb33);
            this.panel1.Controls.Add(this.cb04);
            this.panel1.Controls.Add(this.cb22);
            this.panel1.Controls.Add(this.cb41);
            this.panel1.Controls.Add(this.cb13);
            this.panel1.Controls.Add(this.cb31);
            this.panel1.Controls.Add(this.cb02);
            this.panel1.Controls.Add(this.cb23);
            this.panel1.Controls.Add(this.cb11);
            this.panel1.Controls.Add(this.cb21);
            this.panel1.Controls.Add(this.cb03);
            this.panel1.Controls.Add(this.cb01);
            this.panel1.Location = new System.Drawing.Point(6, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(237, 111);
            this.panel1.TabIndex = 0;
            // 
            // tb0
            // 
            this.tb0.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb0.Location = new System.Drawing.Point(84, 2);
            this.tb0.Name = "tb0";
            this.tb0.ReadOnly = true;
            this.tb0.Size = new System.Drawing.Size(41, 15);
            this.tb0.TabIndex = 2;
            this.tb0.Text = "0x00";
            this.tb0.TextChanged += new System.EventHandler(this.tb0_TextChanged);
            // 
            // tb1
            // 
            this.tb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb1.Location = new System.Drawing.Point(84, 18);
            this.tb1.Name = "tb1";
            this.tb1.ReadOnly = true;
            this.tb1.Size = new System.Drawing.Size(41, 15);
            this.tb1.TabIndex = 2;
            this.tb1.Text = "0x20";
            this.tb1.TextChanged += new System.EventHandler(this.tb0_TextChanged);
            // 
            // tb2
            // 
            this.tb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb2.Location = new System.Drawing.Point(84, 33);
            this.tb2.Name = "tb2";
            this.tb2.ReadOnly = true;
            this.tb2.Size = new System.Drawing.Size(41, 15);
            this.tb2.TabIndex = 2;
            this.tb2.Text = "0x40";
            this.tb2.TextChanged += new System.EventHandler(this.tb0_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(84, 36);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(41, 15);
            this.textBox4.TabIndex = 2;
            // 
            // tb3
            // 
            this.tb3.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb3.Location = new System.Drawing.Point(84, 49);
            this.tb3.Name = "tb3";
            this.tb3.ReadOnly = true;
            this.tb3.Size = new System.Drawing.Size(41, 15);
            this.tb3.TabIndex = 2;
            this.tb3.Text = "0x60";
            this.tb3.TextChanged += new System.EventHandler(this.tb0_TextChanged);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(84, 52);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(41, 15);
            this.textBox3.TabIndex = 2;
            // 
            // tb4
            // 
            this.tb4.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb4.Location = new System.Drawing.Point(84, 66);
            this.tb4.Name = "tb4";
            this.tb4.ReadOnly = true;
            this.tb4.Size = new System.Drawing.Size(41, 15);
            this.tb4.TabIndex = 2;
            this.tb4.Text = "0x80";
            this.tb4.TextChanged += new System.EventHandler(this.tb0_TextChanged);
            // 
            // textBoxChar
            // 
            this.textBoxChar.Location = new System.Drawing.Point(0, 86);
            this.textBoxChar.Name = "textBoxChar";
            this.textBoxChar.Size = new System.Drawing.Size(234, 20);
            this.textBoxChar.TabIndex = 2;
            this.textBoxChar.Text = "0x00, 0x20, 0x40, 0x60, 0x80,";
            // 
            // btReset
            // 
            this.btReset.Location = new System.Drawing.Point(131, 0);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(103, 80);
            this.btReset.TabIndex = 2;
            this.btReset.Text = "Reset";
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // cb20
            // 
            this.cb20.AutoSize = true;
            this.cb20.Location = new System.Drawing.Point(3, 34);
            this.cb20.Name = "cb20";
            this.cb20.Size = new System.Drawing.Size(15, 14);
            this.cb20.TabIndex = 1;
            this.cb20.UseVisualStyleBackColor = true;
            this.cb20.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb40
            // 
            this.cb40.AutoSize = true;
            this.cb40.Location = new System.Drawing.Point(3, 66);
            this.cb40.Name = "cb40";
            this.cb40.Size = new System.Drawing.Size(15, 14);
            this.cb40.TabIndex = 1;
            this.cb40.UseVisualStyleBackColor = true;
            this.cb40.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb30
            // 
            this.cb30.AutoSize = true;
            this.cb30.Location = new System.Drawing.Point(3, 50);
            this.cb30.Name = "cb30";
            this.cb30.Size = new System.Drawing.Size(15, 14);
            this.cb30.TabIndex = 1;
            this.cb30.UseVisualStyleBackColor = true;
            this.cb30.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb10
            // 
            this.cb10.AutoSize = true;
            this.cb10.Location = new System.Drawing.Point(3, 19);
            this.cb10.Name = "cb10";
            this.cb10.Size = new System.Drawing.Size(15, 14);
            this.cb10.TabIndex = 1;
            this.cb10.UseVisualStyleBackColor = true;
            this.cb10.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb44
            // 
            this.cb44.AutoSize = true;
            this.cb44.Location = new System.Drawing.Point(63, 66);
            this.cb44.Name = "cb44";
            this.cb44.Size = new System.Drawing.Size(15, 14);
            this.cb44.TabIndex = 1;
            this.cb44.UseVisualStyleBackColor = true;
            this.cb44.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb42
            // 
            this.cb42.AutoSize = true;
            this.cb42.Location = new System.Drawing.Point(33, 66);
            this.cb42.Name = "cb42";
            this.cb42.Size = new System.Drawing.Size(15, 14);
            this.cb42.TabIndex = 1;
            this.cb42.UseVisualStyleBackColor = true;
            this.cb42.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb34
            // 
            this.cb34.AutoSize = true;
            this.cb34.Location = new System.Drawing.Point(63, 50);
            this.cb34.Name = "cb34";
            this.cb34.Size = new System.Drawing.Size(15, 14);
            this.cb34.TabIndex = 1;
            this.cb34.UseVisualStyleBackColor = true;
            this.cb34.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb32
            // 
            this.cb32.AutoSize = true;
            this.cb32.Location = new System.Drawing.Point(33, 50);
            this.cb32.Name = "cb32";
            this.cb32.Size = new System.Drawing.Size(15, 14);
            this.cb32.TabIndex = 1;
            this.cb32.UseVisualStyleBackColor = true;
            this.cb32.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb14
            // 
            this.cb14.AutoSize = true;
            this.cb14.Location = new System.Drawing.Point(63, 19);
            this.cb14.Name = "cb14";
            this.cb14.Size = new System.Drawing.Size(15, 14);
            this.cb14.TabIndex = 1;
            this.cb14.UseVisualStyleBackColor = true;
            this.cb14.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb24
            // 
            this.cb24.AutoSize = true;
            this.cb24.Location = new System.Drawing.Point(63, 34);
            this.cb24.Name = "cb24";
            this.cb24.Size = new System.Drawing.Size(15, 14);
            this.cb24.TabIndex = 1;
            this.cb24.UseVisualStyleBackColor = true;
            this.cb24.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb43
            // 
            this.cb43.AutoSize = true;
            this.cb43.Location = new System.Drawing.Point(48, 66);
            this.cb43.Name = "cb43";
            this.cb43.Size = new System.Drawing.Size(15, 14);
            this.cb43.TabIndex = 1;
            this.cb43.UseVisualStyleBackColor = true;
            this.cb43.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb12
            // 
            this.cb12.AutoSize = true;
            this.cb12.Location = new System.Drawing.Point(33, 19);
            this.cb12.Name = "cb12";
            this.cb12.Size = new System.Drawing.Size(15, 14);
            this.cb12.TabIndex = 1;
            this.cb12.UseVisualStyleBackColor = true;
            this.cb12.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb33
            // 
            this.cb33.AutoSize = true;
            this.cb33.Location = new System.Drawing.Point(48, 50);
            this.cb33.Name = "cb33";
            this.cb33.Size = new System.Drawing.Size(15, 14);
            this.cb33.TabIndex = 1;
            this.cb33.UseVisualStyleBackColor = true;
            this.cb33.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb04
            // 
            this.cb04.AutoSize = true;
            this.cb04.Location = new System.Drawing.Point(63, 3);
            this.cb04.Name = "cb04";
            this.cb04.Size = new System.Drawing.Size(15, 14);
            this.cb04.TabIndex = 1;
            this.cb04.UseVisualStyleBackColor = true;
            this.cb04.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // cb22
            // 
            this.cb22.AutoSize = true;
            this.cb22.Location = new System.Drawing.Point(33, 34);
            this.cb22.Name = "cb22";
            this.cb22.Size = new System.Drawing.Size(15, 14);
            this.cb22.TabIndex = 1;
            this.cb22.UseVisualStyleBackColor = true;
            this.cb22.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb41
            // 
            this.cb41.AutoSize = true;
            this.cb41.Location = new System.Drawing.Point(18, 66);
            this.cb41.Name = "cb41";
            this.cb41.Size = new System.Drawing.Size(15, 14);
            this.cb41.TabIndex = 1;
            this.cb41.UseVisualStyleBackColor = true;
            this.cb41.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb13
            // 
            this.cb13.AutoSize = true;
            this.cb13.Location = new System.Drawing.Point(48, 19);
            this.cb13.Name = "cb13";
            this.cb13.Size = new System.Drawing.Size(15, 14);
            this.cb13.TabIndex = 1;
            this.cb13.UseVisualStyleBackColor = true;
            this.cb13.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb31
            // 
            this.cb31.AutoSize = true;
            this.cb31.Location = new System.Drawing.Point(18, 50);
            this.cb31.Name = "cb31";
            this.cb31.Size = new System.Drawing.Size(15, 14);
            this.cb31.TabIndex = 1;
            this.cb31.UseVisualStyleBackColor = true;
            this.cb31.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb02
            // 
            this.cb02.AutoSize = true;
            this.cb02.Location = new System.Drawing.Point(33, 3);
            this.cb02.Name = "cb02";
            this.cb02.Size = new System.Drawing.Size(15, 14);
            this.cb02.TabIndex = 1;
            this.cb02.UseVisualStyleBackColor = true;
            this.cb02.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // cb23
            // 
            this.cb23.AutoSize = true;
            this.cb23.Location = new System.Drawing.Point(48, 34);
            this.cb23.Name = "cb23";
            this.cb23.Size = new System.Drawing.Size(15, 14);
            this.cb23.TabIndex = 1;
            this.cb23.UseVisualStyleBackColor = true;
            this.cb23.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb11
            // 
            this.cb11.AutoSize = true;
            this.cb11.Location = new System.Drawing.Point(18, 19);
            this.cb11.Name = "cb11";
            this.cb11.Size = new System.Drawing.Size(15, 14);
            this.cb11.TabIndex = 1;
            this.cb11.UseVisualStyleBackColor = true;
            this.cb11.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb21
            // 
            this.cb21.AutoSize = true;
            this.cb21.Location = new System.Drawing.Point(18, 34);
            this.cb21.Name = "cb21";
            this.cb21.Size = new System.Drawing.Size(15, 14);
            this.cb21.TabIndex = 1;
            this.cb21.UseVisualStyleBackColor = true;
            this.cb21.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb03
            // 
            this.cb03.AutoSize = true;
            this.cb03.Location = new System.Drawing.Point(48, 3);
            this.cb03.Name = "cb03";
            this.cb03.Size = new System.Drawing.Size(15, 14);
            this.cb03.TabIndex = 1;
            this.cb03.UseVisualStyleBackColor = true;
            this.cb03.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // cb01
            // 
            this.cb01.AutoSize = true;
            this.cb01.Location = new System.Drawing.Point(18, 3);
            this.cb01.Name = "cb01";
            this.cb01.Size = new System.Drawing.Size(15, 14);
            this.cb01.TabIndex = 1;
            this.cb01.UseVisualStyleBackColor = true;
            this.cb01.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // cb00
            // 
            this.cb00.AutoSize = true;
            this.cb00.Location = new System.Drawing.Point(9, 15);
            this.cb00.Name = "cb00";
            this.cb00.Size = new System.Drawing.Size(15, 14);
            this.cb00.TabIndex = 1;
            this.cb00.UseVisualStyleBackColor = true;
            this.cb00.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 128);
            this.Controls.Add(this.cb00);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "SCDQ Char generator";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb0;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox tb4;
        private System.Windows.Forms.TextBox textBoxChar;
        private System.Windows.Forms.Button btReset;
        private System.Windows.Forms.CheckBox cb20;
        private System.Windows.Forms.CheckBox cb40;
        private System.Windows.Forms.CheckBox cb30;
        private System.Windows.Forms.CheckBox cb10;
        private System.Windows.Forms.CheckBox cb44;
        private System.Windows.Forms.CheckBox cb42;
        private System.Windows.Forms.CheckBox cb34;
        private System.Windows.Forms.CheckBox cb32;
        private System.Windows.Forms.CheckBox cb14;
        private System.Windows.Forms.CheckBox cb24;
        private System.Windows.Forms.CheckBox cb43;
        private System.Windows.Forms.CheckBox cb12;
        private System.Windows.Forms.CheckBox cb33;
        private System.Windows.Forms.CheckBox cb04;
        private System.Windows.Forms.CheckBox cb22;
        private System.Windows.Forms.CheckBox cb41;
        private System.Windows.Forms.CheckBox cb13;
        private System.Windows.Forms.CheckBox cb31;
        private System.Windows.Forms.CheckBox cb02;
        private System.Windows.Forms.CheckBox cb23;
        private System.Windows.Forms.CheckBox cb11;
        private System.Windows.Forms.CheckBox cb21;
        private System.Windows.Forms.CheckBox cb03;
        private System.Windows.Forms.CheckBox cb01;
        private System.Windows.Forms.CheckBox cb00;
    }
}

